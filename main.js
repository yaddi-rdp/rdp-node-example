const express    = require('express')
const moment     = require('moment')
const bodyParser = require('body-parser')
const helper     = require('./helper');
const signature  = require('./signature');
const sha512     = require('js-sha512')
const is         = require('is_js')
const _          = require('underscore')
const config     = require('./credential.config');
const auto       = require('./autoredirect')
const app        = express()

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var credential = config.get()
var cards = config.card('mbbamex1')
var autotest = false

var mid        = credential.mid;
var secret_key = credential.secret_key;
var ccy        = credential.ccy;
var api_url    = config.urls(credential.env)


/**
 * For Hosted order page (HOP) card detail is inputted in RDP payment page
 * can support redirection_hosted, sop, direct_n3d
 */
app.post('/payment', function (req, res) {
  req.body.mid = mid
  req.body.secret_key = secret_key
  req.body.ccy = ccy

  console.log("")
  console.log("****************** PAYMENTS ******************")
  console.log("********** " +moment().format('YYYY-MM-DD, HH:mm:ss.SSS')+ " **********")

  payment = require('./modules/payment')

  async function f() {
    try {
      var result = await payment.processing(req, api_url);
      console.log(result)
      res.send(result)
    } catch (error) {
      console.log(error)
      res.send(error);
    }
  }

  f().then((val) => {
    res.send(val)
  })

  // d = payment.processing(req, api_url);
  // res.send(d)
})

/**
 * Process the inquiry during payment redirection
 */
app.get('/payment_redirect', function (req, res) {
  console.log("")
  console.log("PAYMENTS REDIRECTION HANDLING")
  console.log(moment().format('YYYY-MM-DD, HH:mm:ss.SSS'))

  var payment = require('./modules/payment')

  async function f() {
    try {
      var result = await payment.redirect(req, api_url);
      console.log(result)
      res.send(result)
    } catch (error) {
      console.log(error)
      res.send(error);
    }
  }

  f().then((val) => {
    res.send(val)
  })
})

app.post('/token', function (req, res, next) {
  req.body.mid = mid
  req.body.secret_key = secret_key
  console.log("")
  console.log("*************** TOKEN HOSTED ***************")
  console.log("********** " +moment().format('MMM Do YYYY, h:mm:ss a')+ " **********")
  // req.body.order_id  = helper.randomString('timestamp')
  req.body.signature = signature.generateSignature(req.body)

  delete req.body['secret_key']
  console.log(helper.ksort(req.body))

  var request = require('request');
  request(
    {
      url    : api_url.token,
      method : 'POST',
      json   : req.body
    },
    function optionalCallback(err, httpResponse, body) {
      if (err) {
        return console.error(err);
      }
      res.send(body)
    }
  );
})

app.get('/token_redirect', function (req, res) {
  console.log("")
  console.log("TOKEN REDIRECTION HANDLING")
  console.log(moment().format('MMMM Do YYYY, h:mm:ss a'))
  data = {};
  data.request_mid    = mid
  data.secret_key     = secret_key
  data.transaction_id = req.query.transaction_id
  data.signature      = signature.signGeneric(data.secret_key, data);

  var request = require('request');
  request(
    {
      url: api_url.token_redirect,
      method : 'POST',
      json   : data
    },
    function optionalCallback(err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      // should be redirect the app.get('/redirected', ...)

      res.send(body)
    }
  );
})

/**
 * Merchant Capture
 */
app.post('/merchant', function(req, res, next) {
  console.log("")
  switch(req.body.action_type) {
    case "capture": console.log("************** MERCHANT - CAPTURE **************")
    break;
    case "refund": console.log("*************** MERCHANT - REFUND **************")
    break;
    default: console.log("**************** MERCHANT - VOID ***************")
  }
  console.log("*********** " +moment().format('YYYY-MM-DD, HH:mm:ss.SSS')+ " ***********")

  req.body.mid = mid
  req.body.secret_key = secret_key
  req.body.signature = signature.md5Signature(req.body);

  delete req.body['secret_key'];
  console.log(req.body)

  var request = require('request');

  request(
    {
      url: api_url.merchant_api,
      method: 'POST',
      form: helper.ksort(req.body)
    },
    function optionalCallback(err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      // should be redirect the app.get('/redirected', ...)
      res.send(body)
    }
  );

})

/**
 * ps: presignature
 * Generate pre signature string. A long string before we hash it with sha512
 */
app.post('/ps', function(req,res) {
  console.log(req.body);
  delete(req.body.signature)
  var data = signature.flatString(req.body);
  res.send(data)
})


// gs: generic signature
// Generate signature for payment api
// Can be use to generate signature and request parameters for payment API
app.post('/gs', function(req,res) {
  console.log("")
  console.log("************ GENERATING SIGNATURE ************")
  console.log("********** " +moment().format('YYYY-MM-DD, HH:mm:ss.SSS')+ " **********")

  var pre_signature = req.body.mid.trim() +
    req.body.order_id.trim() +
    req.body.payment_type +
    req.body.amount + req.body.ccy;

  var cvv_1 = ""
  if (is.existy(req.body.payer_id)) {
    pre_signature += req.body.payer_id
  }

  if (req.body.api_mode == 'redirection_sop' || req.body.api_mode == 'direct_n3d') {
    if (is.existy(req.body.cvv2)) {
      cvv_1 = req.body.cvv2.slice(-1)
    }

    if (is.existy(req.body.payer_id)) {
      pre_signature += cvv_1
    } else {
      pre_signature += req.body.card_no.slice(0, 6) + req.body.card_no.slice(-4) + req.body.exp_date + cvv_1
    }
  }

  pre_signature += req.body.secret_key
  var signature = sha512(pre_signature)

  req.body.signature = signature
  delete req.body['secret_key']
  console.log(helper.ksort(req.body))

  data = req.body
  res.send(data)
})


/**
 * ts: token signature
 * generate generic signature, work for generating signature for token
 */
app.post('/ts', function(req, res) {
  req.body.signature = signature.generateSignature(req.body)
  delete(req.body.secret_key)
  sorted_data  = helper.ksort(req.body)
  console.log(sorted_data);

  res.send(sorted_data)

})

/**
 * Validate signature
 */
app.post('/vs', function(req,res) {
  secret_key = ''
  if(req.body.secret_key) {
    secret_key = req.body.secret_key
  }
  // console.log('mid: ', mid);
  // console.log('secret_key: ', secret_key);
  delete(req.body.signature)
  sign = signature.isValidSignature(req.body, secret_key)
  console.log(sign);
  res.send(sign)
})

app.post('/hash', function(req,res) {
  var hash = sha512(req.body.data)
  var signature = sha512(hash)

  console.log(signature);
  res.send(signature)
})

app.get('/notify', function (req, res) {
  res.send('notify')
})

app.get('/back', function (req, res) {
  var payment = require('./modules/payment')
  payment.redirect(req, res, api_url);
  res.send('back')
})


/**
 * Instant collect save
 */
app.post('/create_invoice', function (req, res, next) {
  console.log("")
  console.log("INSTANT COLLECT API - CREATE INVOICE")
  console.log(moment().format('MMM Do YYYY, h:mm:ss a'))

  req.body.mid = mid
  req.body.secret_key = secret_key
  req.body.created_date = moment().format('YYYY-MM-DD HH:mm:ss')
  req.body.invoice_number = helper.randomString()
  // req.body.expiry_datetime = moment().add(3, 'hours').format('YYYY-MM-DD HH:mm:ss')
  req.body.subject = 'RDP/INTCOLL/' + moment().format('YYYY/MM/DD/HHmmss')

  // console.log(data)
  req.body.signature = signature.generateSignatureInstantCollect(req.body)

  delete req.body['secret_key']
  console.log(req.body)
  var request = require('request');
  request(
    {
      url: api_url.ic_save,
      method: 'POST',
      form: req.body
    },
    function optionalCallback(err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      // should be redirect the app.get('/redirected', ...)
      console.log(body)
      res.send(body)
    }
  );
})

app.post('/send_invoice', function (req, res) {

  console.log("")
  console.log("INSTANT COLLECT API - SENDING INVOICE")
  console.log(moment().format('MMM Do YYYY, h:mm:ss a'))

  req.body.mid = mid
  req.body.secret_key = secret_key
  req.body.signature = signature.generateSignatureInstantCollect(req.body)

  console.log(req.body)
  var request = require('request');
  request(
    {
      url: api_url.ic_send,
      method: 'POST',
      form: req.body
    },
    function optionalCallback(err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      // should be redirect the app.get('/redirected', ...)
      console.log(body)
      res.send(body)
    }
  );
})

app.post('/sms_invoice', function (req, res) {

  console.log("")
  console.log("INSTANT COLLECT API - SENDING SMS")
  console.log(moment().format('MMM Do YYYY, h:mm:ss a'))

  req.body.mid = mid
  req.body.secret_key = secret_key
  req.body.signature = signature.generateSignatureInstantCollect(req.body)

  console.log(req.body)
  var request = require('request');
  request(
    {
      url: api_url.ic_send_sms,
      method: 'POST',
      form: req.body
    },
    function optionalCallback(err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      // should be redirect the app.get('/redirected', ...)
      console.log(body)
      res.send(body)
    }
  );
})

app.post('/invoice_enquiry', function (req, res) {
  console.log("")
  console.log("INSTANT COLLECT API - INVOICE INQUIRY")
  console.log(moment().format('MMM Do YYYY, h:mm:ss a'))

  req.body.request_mid = mid
  req.body.secret_key = secret_key
  req.body.signature = signature.generateSignatureInstantCollect(req.body)

  delete req.body['secret_key']
  console.log(req.body)

  var request = require('request');
  request(
    {
      url: api_url.ic_enquiry,
      method: 'POST',
      form: req.body
    },
    function optionalCallback(err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      // should be redirect the app.get('/redirected', ...)

      console.log(body)
      res.send(body)
    }
  );
})

app.listen(3000);
