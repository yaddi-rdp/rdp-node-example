let moment = require('moment');
let is     = require('is_js');
let _      = require('underscore')
var MongoClient = require('mongodb').MongoClient;

// yaddi:m5Vq4OtQKZ4n60G7
var dbUri = "mongodb+srv://yaddi:m5Vq4OtQKZ4n60G7@cluster0-bqnlp.mongodb.net/test?retryWrites=true";

module.exports = {
  randomString: function (type) {
    switch (type) {
      case 'timestamp':
        return moment().valueOf().toString();
        break;
      default:
        return 'RDP' + this.randomString2(6)
        break;
    }
  },

  randomString2: function(length) {
    var smallcase = 'qwertyuiopasdfghjklzxcvbnm';
    var UPPERcase = 'QWERTYUIOPASDFGHJKLZXCVBNM';
    var number = '1234567890';
    var strings = smallcase + number;

    var randomString = '';
    for (let index = 0; index < length; index++) {
      min = Math.ceil(index)
      max = Math.floor(strings.length)
      randomString += strings[Math.floor(Math.random() * (max - min) + min)]
    }
    return randomString
  },

  ksort: function sortObject(object){
    var sortedObj = {},
        keys = _.keys(object);

    keys = _.sortBy(keys, function(key){
        return key;
    });

    _.each(keys, function(key) {
        if(is.object(object[key])){
          sortedObj[key] = sortObject(object[key]);
        } else {
          sortedObj[key] = object[key];
        }
    });
    return sortedObj;
  },

  dbSave: function (data) {
    var insertedId = null;
    (async function() {
      let client;
      try {
        client = await MongoClient.connect(dbUri);
        console.log("Connected correctly to server");
        const db = client.db("rdpApi");

        // Insert a single document
        let r = await db.collection('apiRequest').insertOne(
          {data: data, requestTimestamp: new Date()},
          function(err, data) {
            insertedId = data.insertedId
          }
        );
      } catch (err) {
        console.log(err.stack);
      }
      client.close();
    })();
  }
};
