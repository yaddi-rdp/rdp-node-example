const signature   = require('../signature');
const helper      = require('../helper');
const httprequest = require('./httprequest');
const request     = require('request');
const reqprom     = require('request-promise');
const sha512      = require('js-sha512')
const is          = require('is_js')
const _           = require('underscore')
const config      = require('../credential.config');


module.exports = {

  processing: function (req, api_url) {
    req.body.order_id = helper.randomString()
    req.body.signature = signature.paymentSignature(req.body)
    delete req.body['secret_key']

    console.log(helper.ksort(req.body))

    request_option = {
      url: api_url.payment,
      method: 'POST',
      json: req.body
    }

    return new Promise((resolve, reject)=> {
      reqprom(request_option)
        .then((api_response) => {
          resolve(api_response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  redirect: function(req, api_url) {
    var credential = config.get()

    data = {};
    if (is.existy(req.body.mid)) {
      data.request_mid = req.body.mid
    } else {
      data.request_mid = credential.mid
    }
    if (is.existy(req.body.secret_key)) {
      data.secret_key = req.body.secret_key
    } else {
      data.secret_key = credential.secret_key

    }
    data.transaction_id = req.query.transaction_id
    data.signature = signature.signGeneric(data.secret_key, data);

    request_option  = {
      url: api_url.payment_redirect,
      method: 'POST',
      json: data
    }

    return new Promise((resolve, reject) => {
      reqprom(request_option)
        .then((api_response) => {
          resolve(api_response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  jsonReq: async function(req, api_url) {
    try {
      return await httprequest.json(req, api_url);
    } catch (error) {
       return error
    }
  }
}
