// Use at least Nodemailer v4.1.0
const nodemailer = require('nodemailer');

// Create a SMTP transporter object
const transporter = nodemailer.createTransport({
	host: 'smtp.ethereal.email',
	port: 587,
	auth: {
		user: 'walter.gleason61@ethereal.email',
		pass: 'NZGPVxdrHDBd9vwT3s'
	}
});

// Message object
let message = {
	from    : 'Sender Name <sender@example.com>',
	to      : 'Recipient <recipient@example.com>',
	subject : 'Nodemailer is unicode friendly ✔',
	text    : 'Hello to myself!',
	html    : '<p><b>Hello</b> to myself!</p>'
};

transporter.sendMail(message, (err, info) => {
	if (err) {
		console.log('Error occurred. ' + err.message);
		return process.exit(1);
	}

	console.log('Message sent: %s', info.messageId);
	// Preview only available when sending through an Ethereal account
	console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
});
