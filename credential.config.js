module.exports = {
  get: function(index = 'default') {

    index = 'default'
    var credential = {
      "default": {
        "mid": "1007778582",
        "secret_key": "757orSx2GDliYSo2ZPMqJ4TauPmdAJwCooUbDMaDhTX4zDk7wk2gXZ774YfBNec6Xuq8GfISAZWJtjGR4jyZK3EY8HCIKlSBAE2cevN8gbjMF8XcVcJa6SWTATxkvTyv",
        "ccy": "SGD",
        "env": "test",
        "amount": 1
      }
    }

    return credential[index]
  },

  urls: function(env = 'test') {
    var urls = {
      'live': {
        'payment': 'https://secure.reddotpayment.com/service/payment-api',
        'payment_redirect': 'https://secure.reddotpayment.com/service/Merchant_processor/query_redirection',
        'token': 'https://secure.reddotpayment.com/service/token-api',
        'token_redirect': 'https://secure.reddotpayment.com/service/Merchant_processor/query_token_redirection',
        'ic_save': 'https://connect.reddotpayment.com/instanpanel/api/instancollect/save',
        'ic_send': 'https://connect.reddotpayment.com/instanpanel/api/instancollect/send',
        'ic_send_sms': 'https://connect.reddotpayment.com/instanpanel/api/instancollect/send_sms',
        'ic_enquiry': 'https://connect.reddotpayment.com/instanpanel/api/instancollect/enquiry',
        'merchant_api': 'https://connect.reddotpayment.com/instanpanel/api/payment'
      },
      'test': {
        'payment': 'https://secure-dev.reddotpayment.com/service/payment-api',
        'payment_redirect': 'https://secure-dev.reddotpayment.com/service/Merchant_processor/query_redirection',
        'token': 'https://secure-dev.reddotpayment.com/service/token-api',
        'token_redirect': 'https://secure-dev.reddotpayment.com/service/Merchant_processor/query_token_redirection',
        'ic_save': 'https://test.reddotpayment.com/instanpanel/api/instancollect/save',
        'ic_send': 'https://test.reddotpayment.com/instanpanel/api/instancollect/send',
        'ic_send_sms': 'https://test.reddotpayment.com/instanpanel/api/instancollect/send_sms',
        'ic_enquiry': 'https://test.reddotpayment.com/instanpanel/api/instancollect/enquiry',
        'merchant_api': 'https://test.reddotpayment.com/instanpanel/api/payment'
      },
      'nanda': {
        'payment': 'https://secure-dev.reddotpayment.com/service-nanda/payment-api',
        'payment_redirect': 'https://secure-dev.reddotpayment.com/service-nanda/Merchant_processor/query_redirection',
        'token': 'https://secure-dev.reddotpayment.com/service-nanda/token-api',
        'token_redirect': 'https://secure-dev.reddotpayment.com/service-nanda/Merchant_processor/query_token_redirection',
        'ic_save': 'https://test.reddotpayment.com/instanpanel/api/instancollect/save',
        'ic_send': 'https://test.reddotpayment.com/instanpanel/api/instancollect/send',
        'ic_send_sms': 'https://test.reddotpayment.com/instanpanel/api/instancollect/send_sms',
        'ic_enquiry': 'https://test.reddotpayment.com/instanpanel/api/instancollect/enquiry',
        'merchant_api': 'https://test.reddotpayment.com/instanpanel/api/payment'
      }
    }

    return urls[env]
  },

  card: function(index = '4111') {
    var cards = {
      "4111":{"card_no":"4111111111111111","cvv":"123","exp_month":"11","exp_year":"2023","card_type":"Visa"}
    }

    return cards[index]
  }
}
